const path = require("path");
module.exports = {
  css: {
    loaderOptions: {
      stylus: {
        'resolve url': true,
        'import': [
          './src/theme'
        ]
      }
    }
  },
  lintOnSave: false,
  pluginOptions: {
    'cube-ui': {
      postCompile: true,
      theme: true
    },
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        //加上自己的文件路径，不能使用别名
        path.resolve(__dirname, 'src/style/_var.scss'),
        path.resolve(__dirname, 'src/style/mixin.scss'),
      ]
    }
  }
}
